import { Component } from '@angular/core';
import { PageDataService } from 'src/app/services/page.service';

@Component({
  selector: 'cmail-header',
  templateUrl: './header.component.html',
  styleUrls: [
    './header.component.css',
    './header-search.css'
  ]
})
export class HeaderComponent {

  isMenuOpen = false;

  tituloDaPagina = 'Cmail'

  constructor(private pageService: PageDataService){
    
    this.pageService
      .titulo
      .subscribe(novoTitulo => this.tituloDaPagina = novoTitulo)
  }

  toggleMenu() {
   this.isMenuOpen = !this.isMenuOpen;
  }

}

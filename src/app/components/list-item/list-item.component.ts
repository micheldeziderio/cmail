import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cmail-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})


export class ListItemComponent implements OnInit {
  
  @Input() destinatario = "";
  @Input() assunto = "";
  @Input() conteudo = "";
  @Input() dataEnvio = "";
  
  @Output('eventoVaiRemover') vairRemover = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  removerEmail(click: Event){
    /* console.log('clicou no botão remover') */

    if(confirm('Tem Certeza?')){
      this.vairRemover.emit({ status: 'removing' })
    }
  }

}

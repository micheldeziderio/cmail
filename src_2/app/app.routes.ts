import { Routes, RouterModule } from "@angular/router";
import { CaixaDeEntradaComponent } from './modulos/caixa-de-entrada/caixa-de-entrada.component';
import { CadastroComponent } from './modulos/cadastro/cadastro.component';
import { LoginComponent } from './modulos/login/login.component';

const rotasApp: Routes = [
    {path: 'cadastro', component: CadastroComponent},
    {path: 'login', component: LoginComponent},
    {path: '', component: CaixaDeEntradaComponent},
    {path: '**', redirectTo: 'login'}
];

export const ModuleRoteamento = RouterModule.forRoot(rotasApp)
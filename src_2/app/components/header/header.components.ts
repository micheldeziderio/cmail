import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'cmail-header',
  templateUrl: './header.component.html',
  styleUrls: ['./search.component.css',
            './header.component.css']
})

export class HeaderComponent {
  constructor (private roteador: Router){}

  isMenuOpen =  false;

  toggleMenu() {
    this.isMenuOpen = !this.isMenuOpen;
  }
  
  logoutInbox() {
    localStorage.removeItem('cmail-token')
    this.roteador.navigate(['login'])
  }
}
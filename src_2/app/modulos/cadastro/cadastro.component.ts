import { Component, OnInit, Input, ɵConsole } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpErrorResponse, HttpResponseBase } from '@angular/common/http';
import { UserInputDTO } from 'src/app/models/dto/user-input';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators'
import { CadastroService } from './cadastro.service';


@Component({
  selector: 'cmail-cadastro',
  templateUrl: './cadastro.component.html',
  styles: []
})
export class CadastroComponent implements OnInit {
  
  private validadoresNome = Validators.compose([
    Validators.required,
    Validators.minLength(2),
    Validators.maxLength(255),
    Validators.pattern('[a-zA-Z\u00C0-\u00FF ]+')
  ])

  private validadoresUsuario = Validators.compose([
    Validators.required,
    Validators.minLength(10),
    Validators.maxLength(50),
    Validators.pattern('[a-zA0-9]+')
  ])

  private validadoresSenha = Validators.compose([
    Validators.required,
    Validators.minLength(6),
    Validators.maxLength(20)
  ])

  private validadoresTelefone = Validators.compose([
    Validators.required,
    Validators.minLength(8),
    Validators.maxLength(9),
    Validators.pattern('[0-9]+')
  ])

  private validadoresFoto = Validators.compose([
    Validators.required
   
  ])

  public nome = new FormControl('', this.validadoresNome)
  public username = new FormControl('', this.validadoresUsuario)
  public senha = new FormControl('', this.validadoresSenha)
  public telefone = new FormControl('', this.validadoresTelefone)
  public avatar = new FormControl('', this.validadoresFoto,  this.validaImagem.bind(this))

  formCadatro = new FormGroup({
    nome: this.nome,
    username: this.username,
    senha: this.senha,
    telefone: this.telefone,
    avatar: this.avatar
  })
  
  mensagem = '';

  constructor(private httpRequest: HttpClient,
            private cadastroService: CadastroService
            ,private roteador: Router ) { }
  
  ngOnInit() {
  }

  validaImagem(campo : FormControl) {

    const erroValidacao = {
      urlValida: true
    };

    return this.httpRequest.head(
      campo.value,
    
      {observe: 'response'}
      
      )
      .pipe(
        map((resposta: HttpResponseBase) =>{
          
          const contentType = resposta.headers.get('Content-Type')

          if(resposta.ok && contentType.includes('image')){

            // null quer dizer que não tem erros de validação
            return null
          }else{
            return erroValidacao
          }
        })
        ,catchError((erro: HttpErrorResponse) => [erroValidacao]
        )
      )
  }

  handleCadastro() {


    if(this.formCadatro.invalid) {
      this.formCadatro.markAllAsTouched()      
      return;
    }

    this.cadastroService.autenticar(this.formCadatro.value)
      .subscribe(
        respota => {

          this.mensagem = "Cadastro feito com sucesso!";
          this.formCadatro.reset();
        }
        ,(erro: HttpErrorResponse) => {
          this.mensagem = erro.error.body[0].message          
        }
        ,() => {
          setTimeout( () => {
            this.roteador.navigate(['login'])
          }, 3000)
        }
      )

  }

}

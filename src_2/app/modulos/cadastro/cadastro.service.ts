import { environment } from 'src/environments/environment'
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserInputDTO } from 'src/app/models/dto/user-input';

@Injectable()

export class CadastroService {
    private url = environment.api + 'users/';

    constructor(private http: HttpClient) { }

    autenticar(CadastroData) {

        const cadastroDTO = new UserInputDTO(CadastroData);
        
        return this.http
            .post(this.url, cadastroDTO)
    }

}
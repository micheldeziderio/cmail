import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { EmailService } from './email.service';

@Component({
  selector: 'cmail-caixa-de-entrada',
  templateUrl: './caixa-de-entrada.component.html',
  styles: []
})
export class CaixaDeEntradaComponent implements OnInit {

  constructor(private emailService: EmailService) { }

  
  listaEmails = [];
  
  email = {
    destinatario : '',
    assunto: '',
    conteudo: ''
  }
  
  // Deixando o elemento privado
  private _isNewEmailOpen = false;
  
  
  ngOnInit() {

    this.emailService
    .listar()
    .subscribe(
      (listaEmailsApi) => {
        this.listaEmails = listaEmailsApi
        /* console.log(this.listaEmails); */
        
      }
      ,(erro) => console.log(erro)
      
    )

  }

  // Criando getter
  get isNewEmailOpen () {
    return this._isNewEmailOpen;
  }

  listarEmails (){
    this.listaEmails.push({
      destinatario: this.email.destinatario,
      assunto: this.email.assunto,
      conteudo: this.email.conteudo
    })
  }

  // Pegando o elemento privado
  toggleNewEmail() {
    this._isNewEmailOpen = !this.isNewEmailOpen;
  }


  handleNewEmail(formEmail: NgForm){

    if(formEmail.invalid){ 
      // disparando evento nem todos os inputs
      formEmail.control.markAllAsTouched()      
      return
    };

    this.emailService
    .enviar(this.email)
    .subscribe(
      (resposta) => {
        /* console.log('deu certo', resposta) */

        
        formEmail.resetForm()

      }
      ,(erro) => {
        console.log('deu ruim' ,  erro);
        
      }
    )
    

  }
}

import { environment } from 'src/environments/environment'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Output } from '@angular/core';
import { EmailInputDTO } from 'src/app/models/dto/email-input';
import { EmailForm } from 'src/app/models/email-form/email-form';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Email } from 'src/app/models/dto/email';
import { EmailOutputDTO } from 'src/app/models/dto/email-output';
 
@Injectable()

export class EmailService {
    private url = environment.api + 'emails/';
    private httpOptions = { headers: new HttpHeaders( {'Authorization': localStorage.getItem('cmail-token') })
}
    
    constructor(private http: HttpClient) { }

    enviar(email: EmailForm) {

        debugger

        const emailDTO: EmailInputDTO = {
            to: email.destinatario,
            subject: email.assunto,
            content: email.conteudo
        }

        return this.http.post(this.url, emailDTO,this.httpOptions)
    }

    listar(): Observable <Email[]> {
        return this.http
            .get<EmailOutputDTO[]>(this.url, this.httpOptions)
            .pipe(
                map(
                    listaEmIngles => {
                        return listaEmIngles.map(
                            emailIngles => {
                                return new Email(emailIngles)
                            }
                        )
                    }
                )
            )
    }
}
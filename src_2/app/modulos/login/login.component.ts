import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'cmail-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private validadoresEmail = Validators.compose([
    Validators.required
  ])

  private validadoresSenha = Validators.compose([
    Validators.required
  ])

  public email = new FormControl('mohamedali@cmail.com.br', this.validadoresEmail)
  public senha = new FormControl('123', this.validadoresSenha)

  formLogin = new FormGroup({
    email: this.email,
    senha: this.senha
  })

  mensagem = '';

  constructor(private loginService: LoginService,
              private roteador: Router) { }

  ngOnInit() {
  }

  handleLogin(){

   

    if(this.formLogin.invalid) {
      this.formLogin.markAllAsTouched()      
      return;
    }

   

      this.loginService.autenticar(this.formLogin.value)
      .subscribe(
        
        (response: any) => {
          console.log(response);
          this.mensagem = 'Entrando...';
          this.formLogin.reset();
          localStorage.setItem('cmail-token', response.token)
        },
        (erro: HttpErrorResponse) => {

          /* this.mensagem = 'Ops, algo deu errado! :('   */
          this.mensagem = erro.error.message
          

        },() => {
          setTimeout( () => {
            this.roteador.navigate(['inbox'])
          }, 3000)
        }
      )

    
  }

}

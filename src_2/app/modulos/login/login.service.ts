import { environment } from 'src/environments/environment'
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginInputDTO } from 'src/app/models/dto/login-input';

@Injectable()

export class LoginService {
    private url = environment.api + 'login/';

    constructor(private http: HttpClient) { }

    autenticar(LoginData) {

        const loginDTO = new LoginInputDTO(LoginData);
        
        return this.http
            .post(this.url, loginDTO)
    }

}